package com.rapp.applications.media.labeler

import org.junit.BeforeClass
import org.junit.Test

import static com.rapp.applications.media.labeler.RetrieveData.*
import static org.junit.Assert.*

class RetrieveDataTest {
	@BeforeClass static def void setShowData() {
		RetrieveData.showName = "Marvel's Agents of S.H.I.E.L.D."
		RetrieveData.seasonNumber = 1
		RetrieveData.episodeNumber = 1
	}

	@Test def void RetrieveShowInfoTest() {
		RetrieveData.retrieveShowInfo

		assertEquals("263365", RetrieveData.showID)
	}

	@Test def void RetrieveEpisodeInfoTest() {
		RetrieveData.retrieveEpisodeInfo
	}
}
