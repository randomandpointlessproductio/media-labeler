package com.rapp.applications.media.labeler

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import org.eclipse.xtend.lib.annotations.Accessors

import static extension com.rapp.libraries.httpurlconnectionextensions.HttpURLConnectionExtensions.*
import static extension com.rapp.libraries.xtendshortcuts.Extensions.*

@Accessors class RetrieveData {
	val static baseURL = "https://api.thetvdb.com"
	public var static String showName
	public var static String showID
	public var static int episodeNumber
	public var static int seasonNumber
	var static String jwtToken = null

	static def encode(String source) { return URLEncoder.encode(source, UTF_8_STRING) }

	static def getJSONData(JsonObject jsonObj) { return jsonObj.get("data").asJsonArray.get(0).asJsonObject }

	static def getJSONField(JsonObject jsonObj, String field) {
		if (jsonObj.has("data")) {
			return jsonObj.getJSONData.get(field)
		} else {
			return jsonObj.get(field)
		}
	}

	static def queryAPI(String uri, String json) {
		val connection = new URL('''«baseURL»/«uri»'''.toString).openConnection as HttpURLConnection => [
			setRequestProperty("Content-Type", "application/json")
			setRequestProperty("Accept", "application/json")
			requestMethod = "POST"
			if (jwtToken != null) {
				val auth = '''Bearer «jwtToken»'''.toString
				setRequestProperty("Authorization", auth)
			}
			doOutput = true
			outputStream.write(json.bytes)
			outputStream.close
		]

		return new JsonParser().parse(connection.checkForErrors.readResponseContent).asJsonObject
	}

	static def queryAPI(String uri) {
		val connection = new URL('''«baseURL»/«uri»'''.toString).openConnection as HttpURLConnection => [
			setRequestProperty("Content-Type", "application/json")
			setRequestProperty("Accept", "application/json")
			requestMethod = "GET"
			if (jwtToken != null) {
				val auth = '''Bearer «jwtToken»'''.toString
				setRequestProperty("Authorization", auth)
			}
		]

		return new JsonParser().parse(connection.checkForErrors.readResponseContent).asJsonObject
	}

	static def authenticate() {
		if (jwtToken == null) {
			println("Authenticating ...")
			val credentialsJson = ClassLoader.getSystemResourceAsStream("credentials.properties").readToStringClose
			jwtToken = queryAPI("login", credentialsJson).getJSONField("token").toString.replace('"', '')
			println("... Token found.")
		}
	}

	static def retrieveShowInfo() {
		retrieveShowInfo(false)
	}

	static def retrieveShowInfo(Boolean force) {
		if (force || showID == null) {
			authenticate
			println('''Retriving info on «showName»''')

			showID = queryAPI('''search/series?name=«showName.encode»'''.toString).getJSONField("id").toString
			println(''' ... Found show. It's ID is «showID»''')
		}
	}

	static def retrieveEpisodeInfo() {
		if (showID == null) {
			retrieveShowInfo
		}
		authenticate
		println('''Retrieving info on «showName»: season #«seasonNumber», episode #«episodeNumber»''')

		println(queryAPI('''series/«showID»/episodes/query?airedSeason=«seasonNumber»&airedEpisode=«episodeNumber»'''))
	}

}
